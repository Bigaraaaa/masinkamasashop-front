import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from './core/auth/role-guard';
import { LoginComponent } from './shared//components/login/login.component';

const routes: Routes = [
  {
    path: 'visitor',
    loadChildren: './modules/visitor/visitor.module#VisitorModule',
    // canActivate: ''
  },
  {
    path: 'admin',
    loadChildren: './modules/admin/admin.module#AdminModule',
    canActivate: [RoleGuard],
    data: { expectedRoles: ['ROLE_ADMINISTRATOR'] },
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '**',
    redirectTo: '/visitor/homepage',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
