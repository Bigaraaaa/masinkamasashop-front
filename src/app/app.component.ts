import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetProducts } from './store/products-store/products-actions';
import { async } from '@angular/core/testing';
import { RootState } from './store/rootstate';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Klementina Shop';
  products;
  user = false;
  username = 'klementinashop';

  constructor(
    private store: Store
  ) { }

   ngOnInit() {
    this.store.dispatch(new GetProducts()).subscribe(async pr => {
      await console.log(pr.products.products);
      this.products = pr.products.products;
    });
    this.store.select((rootState: RootState) => rootState.connection.loggedUser).subscribe((user) => {
      console.log(user);
      this.user = true;
    });
  }

}
