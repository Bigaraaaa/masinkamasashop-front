import {
  Component,
  OnInit,
  Input,
  ElementRef,
  Inject,
  ViewChild,
} from "@angular/core";
import { Store, Select } from "@ngxs/store";
import { AddToCart } from "src/app/store/products-store/products-actions";
import { Product } from "src/app/core/models/product";
import Swal from "sweetalert2";
import { ProductsState } from "src/app/store/products-store/products-state";
import { Observable } from "rxjs";
import { DOCUMENT } from "@angular/common";
import { ViewCartItemComponent } from "src/app/modules/visitor/containers/home/view-cart-item/view-cart-item.component";

@Component({
  selector: "app-product-details-modal",
  templateUrl: "./product-details-modal.component.html",
  styleUrls: ["./product-details-modal.component.css"],
})
export class ProductDetailsModalComponent implements OnInit {
  @Select(ProductsState.getSelectedProduct) product$: Observable<Product[]>;
  @Input() product;
  @ViewChild("closeButton", { static: false }) closeButton: ElementRef;
  selected = false;
  selectedSize = false;
  selectedSize1 = false;
  selectedSize2 = false;
  selectedSize3 = false;
  selectedSize4 = false;
  selectedSize5 = false;
  selectedProduct = {
    name: "",
    description: "",
    amount: 1,
    imageUrl: "",
    // size: this.getSize().amount,
    size: "",
    // price: this.getSize().price,
    price: "",
  };
  sizePrice = {
    size: "",
    price: null,
    // size: this.getSize().amount,
    // price: this.getSize().price,
  };

  constructor(private store: Store) {}

  ngOnInit() {
    this.sizePrice.price = this.product.productDetails[0].size[0].price;
    this.sizePrice.size = this.product.productDetails[0].size[0].amount;
    // this.product$.subscribe((data) => {
    //   if (data["productDetails"][0].size.length === 1) {
    //     this.selected = true;
    //   }
    // });

    console.log(
      { sizePrice: this.sizePrice },
      { selectedProduct: this.product }
    );
  }

  getSize(): any {
    let size = null;
    this.product$.subscribe((data) => {
      size = data["productDetails"][0].size[0];
    });
    return size;
  }

  checkSize() {
    // return this.product$.subscribe(
    //   (data) => data["productDetails"][0].size.length === 1
    // );

    // this.product$.subscribe((data) =>
    //   console.log(data["productDetails"][0].size.length === 1)
    // );

    console.log(this.selectedProduct);
  }

  addToCart() {
    console.log(this.selectedProduct);
    this.store
      .dispatch(new AddToCart(this.selectedProduct))
      .subscribe((pr: any) => {
        Swal.fire({
          position: "bottom-end",
          icon: "success",
          title:
            "Proizvod " +
            this.selectedProduct.name +
            " je uspešno dodat u korpu !",
          showConfirmButton: false,
          timer: 1500,
        });
        this.selectedProduct = {
          name: "",
          description: "",
          amount: 1,
          imageUrl: "",
          size: "",
          price: "",
        };
      });
    this.closeButton.nativeElement.click();
  }

  selectProductSize(size, product) {
    this.selected = true;
    this.sizePrice.price = size.price;
    this.selectedProduct.name = product.name;
    this.selectedProduct.amount = 1;
    this.selectedProduct.description = product.description;
    this.selectedProduct.price = size.price;
    this.selectedProduct.size = size.amount;
    this.selectedProduct.imageUrl = product.imageUrl;
  }
}
