import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Product } from 'src/app/core/models/product';
import { Store, Select } from '@ngxs/store';
import { AddToCart, SelectProduct } from 'src/app/store/products-store/products-actions';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { ProductsState } from 'src/app/store/products-store/products-state';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  @Select(ProductsState.getSelectedProduct) product$: Observable<Product[]>;
  @Input() product: any;

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

  selectProduct() {
    this.store.dispatch(new SelectProduct(this.product));
  }

}
