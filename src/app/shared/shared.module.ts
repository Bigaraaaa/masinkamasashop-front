import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { ProductDetailsModalComponent } from './modals/product-details-modal/product-details-modal.component';



@NgModule({
  declarations: [LoginComponent, NavbarComponent, RegistrationComponent, ProductCardComponent, ProductDetailsModalComponent],
  imports: [
    CommonModule
  ],
  exports: [
    LoginComponent,
    NavbarComponent,
    RegistrationComponent,
    ProductCardComponent,
    ProductDetailsModalComponent
  ]
})
export class SharedModule { }
