import { OrderStatus } from './status';

export interface Orders {
    id?: number;
    name: string;
    surname: string;
    phone: string;
    email: string;
    street: string;
    number: string;
    floor: string;
    apartmant: string;
    additionalInformation: string;
    sum: number;
    status: OrderStatus;
}
