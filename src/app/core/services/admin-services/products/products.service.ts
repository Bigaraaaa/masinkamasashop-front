import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getProducts() {
    return this.httpClient.get<[any]>(environment.apiUrl + `product/all`);
  }

  saveProduct(product, prodDet) {
    return this.httpClient.post<any>(environment.apiUrl + `product/save-product`, prodDet);
  }

  saveSize(id: number, size: any) {
    return this.httpClient.post<any>(environment.apiUrl + `size/addSize/${id}`, size);
  }

  getProduct(id: number) {
    return this.httpClient.get<any>(environment.apiUrl + `product/${id}`);
  }

  saveOrderedProduct(id: any, product: any) {
    return this.httpClient.post<any>(environment.apiUrl + `orders-product/saveProduct/${id}`, product);
  }

}
