import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Orders } from 'src/app/core/models/orders';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getOrders() {
    return this.httpClient.get<[any]>(environment.apiUrl + `orders/all`);
  }

  sendOrder(order: Orders) {
    console.log(order);
    return this.httpClient.post<Orders>(environment.apiUrl + `orders/add`, order);
  }

  fufillOrder(productID: any) {
    console.log({ productID });
    return this.httpClient.put<any>(environment.apiUrl + `orders/fulfillOrder/` + productID, {});
  }

}
