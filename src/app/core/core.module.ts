import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { ConnectionState } from '../store/connection-store/connection-state';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { environment } from 'src/environments/environment';
import { ProductsState } from '../store/products-store/products-state';
import { OrdersState } from '../store/orders-store/orders-state';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxsModule.forRoot([
      ConnectionState,
      ProductsState,
      OrdersState
    ], { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot()
  ]
})
export class CoreModule { }
