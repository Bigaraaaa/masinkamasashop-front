import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { User } from "src/app/core/models/user";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  roleChanged = new Subject<any[]>();
  tokenChanged = new Subject<any[]>();
  loggedInStatusChanged = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) {}

  // tslint:disable-next-line:typedef
  login(em: string, pin: string) {
    this.http
      .post<{ accessToken: string }>(environment.apiUrl + `login`, {
        email: em,
        password: pin,
      })
      .subscribe((response) => {
        if (response.accessToken) {
          localStorage.setItem("accessToken", response.accessToken);
          console.log(response.accessToken);
          this.roleChanged.next(this.getCurrentRoles());
          this.tokenChanged.next(this.getTokenExpired());
          this.router.navigate(["/admin"]);
          this.loggedInStatusChanged.next(true);
        }
      });
  }

  test() {
    const accessToken = localStorage.getItem("accessToken");
    const parsedToken = JSON.parse(atob(accessToken.split(".")[1]));
    console.log(parsedToken.sub);
  }

  // tslint:disable-next-line:typedef
  logout() {
    this.roleChanged.next([]);
    localStorage.removeItem("accessToken");
    this.router.navigate(["/visitor"]);
    this.loggedInStatusChanged.next(false);
  }
  // tslint:disable-next-line:typedef
  getCurrentRoles() {
    const accessToken = localStorage.getItem("accessToken");
    if (accessToken) {
      const parsedToken = JSON.parse(atob(accessToken.split(".")[1]));
      const roles = [];
      if (accessToken) {
        parsedToken.role.forEach((role) => {
          roles.push(role.authority);
        });
      }
      return roles;
    }
  }
  // tslint:disable-next-line:typedef
  getCurrentUser() {
    const accessToken = localStorage.getItem("accessToken");
    if (accessToken) {
      const parsedToken = JSON.parse(atob(accessToken.split(".")[1]));
      if (accessToken) {
        return {
          email: parsedToken.uniq,
          id: parsedToken.sub,
          password: parsedToken.password,
        };
      }
      return null;
    }
  }
  // tslint:disable-next-line:typedef
  getTokenExpired() {
    const accessToken = localStorage.getItem("accessToken");
    if (accessToken) {
      const parsedToken = JSON.parse(atob(accessToken.split(".")[1]));
      if (accessToken) {
        return parsedToken.exp;
      }

      return null;
    }
  }
  // tslint:disable-next-line:typedef
  isLoggedIn() {
    if (localStorage.getItem("accessToken")) {
      return true;
    }
    return false;
  }
}
