import { State, Action, StateContext } from '@ngxs/store';
import { GetLoggedUser, Login, SetLoggedUser } from './connection-actions';

export class ConnectionStateModel {
    loggedUser: { id: null, accountData: null };
}

@State<ConnectionStateModel>({
    name: 'connection',
    defaults: {
        loggedUser: { id: null, accountData: null }
    }
})
export class ConnectionState {

    @Action(GetLoggedUser)
    getCoaches({ patchState }: StateContext<ConnectionStateModel>) {

    }

    @Action(SetLoggedUser)
    setLoggedUser({ patchState }: StateContext<ConnectionStateModel>, { user }: SetLoggedUser) {
        patchState({
            loggedUser: user
        });
    }

    @Action(Login)
    login({ patchState }: StateContext<ConnectionStateModel>, { credentials }: Login) {
        patchState({
            loggedUser: credentials
        });
    }

}
