export class GetLoggedUser {
    static readonly type = '[connection] GetLoggedUser';

    constructor() { }
}

export class SetLoggedUser {
    static readonly type = '[connection] SetLoggedUser';

    constructor(public user: any) { }
}

export class Login {
    static readonly type = '[connection] Login';

    constructor(public credentials: any) { }
}
