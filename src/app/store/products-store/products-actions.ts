import { Product } from 'src/app/core/models/product';

export class GetProducts {
    static readonly type = '[products] GetProducts';

    constructor() { }
}

export class AddToCart {
    static readonly type = '[products] AddToCart';

    constructor(public product: any) { }
}

export class RemoveItem {
    static readonly type = '[products] RemoveItem';

    constructor(public product: any) { }
}

export class SelectProduct {
    static readonly type = '[product] SelectProduct';

    constructor(public product: Product) { }
}

export class SaveProduct {
    static readonly type = '[product] SaveProduct';

    constructor(public product: any, public prodDet: any) { }
}

export class SaveSizes {
    static readonly type = '[product] SaveSizes';

    constructor(public id: number, public size: any) { }
}

export class GetProduct {
    static readonly type = '[product] GetProduct';

    constructor(public id: number) { }
}

export class SaveOrderedProduct {
    static readonly type = '[product] SaveOrderedProduct';

    constructor(public id: any, public product: any) { }
}

export class RemoveAllFromCart {
    static readonly type = '[product] RemoveAllFromCart';

    constructor() { }
}

export class ChangeAmount {
    static readonly type = '[product] ChangeAmount';

    constructor(public value: number, public oldProduct: any, public product: any) { }
}

export class FilterProductCategory {
    static readonly type = '[order] FilterProductCategory';

    constructor(public category: string) { }
}
