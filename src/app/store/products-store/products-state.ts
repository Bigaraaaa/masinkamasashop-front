import { State, Action, StateContext, Selector, Store } from "@ngxs/store";
import {
  AddToCart,
  RemoveItem,
  GetProducts,
  SelectProduct,
  SaveProduct,
  SaveSizes,
  GetProduct,
  SaveOrderedProduct,
  RemoveAllFromCart,
  ChangeAmount,
  FilterProductCategory,
} from "./products-actions";
import { Product } from "src/app/core/models/product";
import { ProductsService } from "src/app/core/services/admin-services/products/products.service";
import { tap } from "rxjs/operators";
import Swal from "sweetalert2";

export class ProductsStateModel {
  products = [];
  selectedCategory = [];
  cart = [];
  selectedProduct = {};
  lastAdded = {};
}

@State<ProductsStateModel>({
  name: "products",
  defaults: {
    products: [],
    selectedCategory: [],
    cart: [],
    selectedProduct: {},
    lastAdded: {},
  },
})
export class ProductsState {
  constructor(private store: Store, private productsService: ProductsService) {}

  @Selector()
  static getProducts(state: ProductsStateModel) {
    return state.products;
  }

  @Selector()
  static getSelectedProduct(state: ProductsStateModel) {
    return state.selectedProduct;
  }

  @Selector()
  static getLastAdded(state: ProductsStateModel) {
    return state.lastAdded;
  }

  @Selector()
  static getCart(state: ProductsStateModel) {
    return state.cart;
  }

  @Selector()
  static getSelectedCategory(state: ProductsStateModel) {
    return state.selectedCategory;
  }

  @Action(GetProducts)
  getProducts(
    { patchState }: StateContext<ProductsStateModel>,
    {}: GetProducts
  ) {
    return this.productsService.getProducts().pipe(
      tap((newProducts) => {
        console.log({ products: newProducts });
        patchState({
          products: [...newProducts],
          selectedProduct: newProducts[0],
        });
      })
    );
  }

  @Action(AddToCart)
  addToCart(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { product }: AddToCart
  ) {
    const state = getState();
    patchState({
      cart: [...state.cart, product],
    });
  }

  @Action(RemoveItem)
  removeItem(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { product }: RemoveItem
  ) {
    const state = getState();
    const filteredProduct = state.cart.filter((el) => {
      return el.price !== product.price && el.size !== product.size; // && el.name !== product.name && el.size !== product.size;
    });
    patchState({
      cart: [...filteredProduct],
    });
  }

  @Action(SelectProduct)
  selectProduct(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { product }: SelectProduct
  ) {
    const state = getState();
    console.log(product);
    patchState({
      selectedProduct: product,
    });
  }

  @Action(SaveProduct)
  saveProduct(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { product, prodDet }: SaveProduct
  ) {
    const state = getState();
    console.log(product, prodDet);
    return this.productsService.saveProduct(product, prodDet).pipe(
      tap((el) => {
        Swal.fire({
          position: "bottom",
          icon: "success",
          title: "Uspešno je dodat proizvod: " + prodDet.name,
          showConfirmButton: false,
          timer: 2000,
        });
        this.store.dispatch(new GetProduct(el.id)).subscribe((pr) => {
          console.log({ pr });
        });
        console.log({ lastAdded: el });
        patchState({
          products: [...state.products, el],
          lastAdded: el,
        });
      })
    );
  }

  @Action(SaveSizes)
  saveSizes(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { id, size }: SaveSizes
  ) {
    const state = getState();
    console.log(id, size);
    return this.productsService.saveSize(id, size).pipe(
      tap((el) => {
        console.log({ el });
        // this.store.dispatch(new GetProduct(el.id)).subscribe((pr) => {
        patchState({
          products: [...state.products, el],
        });
        // });
      })
    );
  }

  @Action(GetProduct)
  getProduct(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { id }: GetProduct
  ) {
    const state = getState();
    console.log(id);
    return this.productsService.getProduct(id).pipe(
      tap((el) => {
        console.log({ el });
        patchState({
          lastAdded: el,
        });
      })
    );
  }

  @Action(SaveOrderedProduct)
  saveOrderedProduct(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { id, product }: SaveOrderedProduct
  ) {
    const state = getState();
    return this.productsService.saveOrderedProduct(id, product).pipe(
      tap((el) => {
        console.log({ el });
        patchState({
          lastAdded: el,
        });
      })
    );
  }

  @Action(RemoveAllFromCart)
  removeAllFromCart(
    { patchState }: StateContext<ProductsStateModel>,
    {}: RemoveAllFromCart
  ) {
    patchState({
      cart: [],
    });
  }

  @Action(ChangeAmount)
  changeAmount(
    { getState, setState }: StateContext<ProductsStateModel>,
    { value, oldProduct, product }: ChangeAmount
  ) {
    const state = getState();
    const filteredCart = [...state.cart];
    const prodIndex = state.cart.findIndex((prod) => prod === oldProduct);
    console.log(prodIndex);
    console.log(filteredCart);
    console.log(product);
    filteredCart[prodIndex] = product;
    setState({
      ...state,
      cart: filteredCart,
    });
  }

  @Action(FilterProductCategory)
  filterProductCategory(
    { getState, patchState }: StateContext<ProductsStateModel>,
    { category }: FilterProductCategory
  ) {
    const state = getState();
    const filteredProducts = state.products.filter((product) => {
      return product.productDetails[0].categoryName === category;
    });
    console.log(filteredProducts);
    patchState({
      selectedCategory: [...filteredProducts],
    });
  }
}
