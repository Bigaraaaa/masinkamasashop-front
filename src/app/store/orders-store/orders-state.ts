import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { GetOrders, SendOrder, SelectOrder, FufillOrder } from './orders-actions';
import { OrdersService } from 'src/app/core/services/admin-services/orders/orders.service';
import { Orders } from 'src/app/core/models/orders';
import { OrderStatus } from 'src/app/core/models/status';

export class OrdersStateModel {
    orders = [];
    selectedOrder: Orders = {
        name: '',
        surname: '',
        phone: null,
        email: '',
        street: '',
        number: '',
        floor: '',
        apartmant: '',
        additionalInformation: '',
        status: null,
        sum: 0
    };
    lastOrder: Orders;
}

@State<OrdersStateModel>({
    name: 'orders',
    defaults: {
        orders: [],
        selectedOrder: {
            name: '',
            surname: '',
            phone: '',
            email: '',
            street: '',
            number: '',
            floor: '',
            apartmant: '',
            additionalInformation: '',
            status: null,
            sum: 0
        },
        lastOrder: {
            id: null,
            name: '',
            surname: '',
            phone: '',
            email: '',
            street: '',
            number: '',
            floor: '',
            apartmant: '',
            additionalInformation: '',
            sum: null,
            status: null
        }
    }
})
export class OrdersState {

    constructor(
        private ordersService: OrdersService
    ) { }

    @Selector()
    static getLastOrder(state: OrdersStateModel) {
        return state.lastOrder;
    }

    @Selector()
    static getOrders(state: OrdersStateModel) {
        return state.orders;
    }

    @Selector()
    static getSelectedOrder(state: OrdersStateModel) {
        return state.selectedOrder;
    }

    // @Action(GetProducts)
    // getProducts({ }: StateContext<ProductsStateModel>, { }: GetProducts) {
    //     // patchState({
    //     //     loggedUser: user
    //     // });
    //     console.log('GetProducts');
    // }

    @Action(GetOrders)
    getOrders({ getState, patchState }: StateContext<OrdersStateModel>, { }: GetOrders) {
        return this.ordersService.getOrders().pipe(tap((newOrders) => {
            console.log({ order: newOrders[0] });
            patchState({
                orders: [...newOrders]
            });
        }));
    }

    @Action(SendOrder)
    sendOrder({ getState, patchState }: StateContext<OrdersStateModel>, { order }: SendOrder) {
        return this.ordersService.sendOrder(order).pipe(tap((newOrder) => {
            const orders = getState().orders;
            patchState({
                orders: [orders, newOrder],
                lastOrder: newOrder
            });
        }));
    }

    @Action(SelectOrder)
    selectOrder({ patchState }: StateContext<OrdersStateModel>, { order }: SelectOrder) {
        patchState({
            selectedOrder: order
        });
    }

    @Action(FufillOrder)
    fufillOrder({ getState, setState }: StateContext<OrdersStateModel>, { orderID }: FufillOrder) {
        // const state = getState();
        // const filteredProduct = state.cart.filter((el: Product) => {
        //     return el.id !== productID;
        // });
        // patchState({
        //     cart: [...filteredProduct]
        // });
        return this.ordersService.fufillOrder(orderID).pipe(tap(order => {
            const state = getState();
            const orders = [...state.orders];
            const orderIndex = orders.findIndex(item => item.id === order.id);
            orders[orderIndex] = order;
            setState({
                ...state, orders
            });
        }));
    }

    // @Action(RemoveItem)
    // removeItem({ getState, patchState }: StateContext<ProductsStateModel>, { productID }: RemoveItem) {
    //     const state = getState();
    //     const filteredProduct = state.cart.filter((el: Product) => {
    //         return el.id !== productID;
    //     });
    //     patchState({
    //         cart: [...filteredProduct]
    //     });
    // }



}
