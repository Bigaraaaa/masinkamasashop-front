import { Orders } from 'src/app/core/models/orders';

export class GetOrders {
    static readonly type = '[orders] GetOrders';

    constructor() { }
}

export class SendOrder {
    static readonly type = '[orders] SendOrder';

    constructor(public order: Orders) { }
}

export class SelectOrder {
    static readonly type = '[orders] SelectOrder';

    constructor(public order: Orders) { }
}

export class FufillOrder {
    static readonly type = '[order] FufillOrder';

    constructor(public orderID: any) { }
}
