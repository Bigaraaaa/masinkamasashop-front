import { ConnectionStateModel } from './connection-store/connection-state';
import { ProductsStateModel } from './products-store/products-state';
import { OrdersStateModel } from './orders-store/orders-state';

export class RootState {
    connection: ConnectionStateModel;
    products: ProductsStateModel;
    orders: OrdersStateModel;
}
