import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminModule } from './modules/admin/admin.module';
import { VisitorModule } from './modules/visitor/visitor.module';
import { SharedModule } from './shared/shared.module';
import { AdminRoutingModule } from './modules/admin/admin-routing.module';
import { CoreModule } from './core/core.module';
import { HomeModule } from './modules/visitor/containers/home/home.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    VisitorModule,
    SharedModule,
    AdminRoutingModule,
    CoreModule,
    HomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
