import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ProductsState } from 'src/app/store/products-store/products-state';

@Component({
  selector: 'app-display-products-container',
  templateUrl: './display-products-container.component.html',
  styleUrls: ['./display-products-container.component.css']
})
export class DisplayProductsContainerComponent implements OnInit {
  @Select(ProductsState.getSelectedCategory) $selectedCategory: Observable<any>;
  categoryName;
  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.categoryName = this.activatedRoute.snapshot.paramMap.get('category');
    // this.categoryName = this.activatedRoute.paramMap.subscribe((param) => {
    //   this.categoryName = param.get('category');
    //   console.log(this.categoryName);
    // });
  }

}
