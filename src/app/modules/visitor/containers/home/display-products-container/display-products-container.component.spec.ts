import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayProductsContainerComponent } from './display-products-container.component';

describe('DisplayProductsContainerComponent', () => {
  let component: DisplayProductsContainerComponent;
  let fixture: ComponentFixture<DisplayProductsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayProductsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayProductsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
