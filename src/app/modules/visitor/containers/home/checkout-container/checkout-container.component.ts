import { Component, OnInit } from "@angular/core";
import { Orders } from "src/app/core/models/orders";
import { OrderStatus } from "src/app/core/models/status";
import { Store, Select } from "@ngxs/store";
import { SendOrder } from "src/app/store/orders-store/orders-actions";
import Swal from "sweetalert2";
import { Router } from "@angular/router";

import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { ProductsState } from "src/app/store/products-store/products-state";
import { Observable } from "rxjs";
import {
  RemoveAllFromCart,
  SaveOrderedProduct,
} from "src/app/store/products-store/products-actions";
import { RootState } from "src/app/store/rootstate";
declare let Email: any;
@Component({
  selector: "app-checkout-container",
  templateUrl: "./checkout-container.component.html",
  styleUrls: ["./checkout-container.component.css"],
})
export class CheckoutContainerComponent implements OnInit {
  @Select(ProductsState.getCart) $getCart: Observable<any>;
  order: Orders = {
    name: "",
    surname: "",
    phone: null,
    email: "",
    street: "",
    number: "",
    floor: "",
    apartmant: "",
    additionalInformation: "",
    status: OrderStatus.unfulfilled,
    sum: 0,
  };

  orderForm = this.fb.group({
    name: new FormControl("", Validators.required),
    surname: new FormControl("", Validators.required),
    phone: new FormControl(null, [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
    ]),
    email: new FormControl("", [Validators.required, Validators.email]),
    Delivery: this.fb.group({
      city: new FormControl("", Validators.required),
      street: new FormControl("", Validators.required),
      number: new FormControl("", [
        Validators.required,
        Validators.pattern("^[0-9]*$"),
      ]),
      floor: "",
      apartment: "",
      additionalInformation: "",
    }),
  });

  orderStreet: string;
  orderCity: string;

  constructor(
    private store: Store,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.store
      .select((rootState: RootState) => rootState.products.cart)
      .subscribe((cart) => {
        cart.map((el) => {
          this.order.sum += el.price * el.amount;
        });
      });
  }

  initializeForms() {
    this.orderForm = this.fb.group({});
  }

  get email() {
    return this.orderForm.get("email");
  }

  get name() {
    return this.orderForm.get("name");
  }

  get surname() {
    return this.orderForm.get("surname");
  }

  get phone() {
    return this.orderForm.get("phone");
  }

  get city() {
    return this.orderForm.get("Delivery.city");
  }

  get street() {
    return this.orderForm.get("Delivery.street");
  }

  get number() {
    return this.orderForm.get("Delivery.number");
  }

  sendEmail() {
    console.log();
    Email.send({
      Host: "smtp.elasticemail.com",
      Username: "klementinaradionice@gmail.com",
      Password: "E8C44F17A7D21E7EB409AEC76891D3B867DE",
      To: "Klementinaradionice@gmail.com",
      From: "Klementinaradionice@gmail.com",
      Subject: "Porudžbina",
      Body: `
      <i>Nova porudžbina.</i> <br/> 
      <b> Ime i prezime poručioca: <b/> ${this.orderForm.value.name} ${this.orderForm.value.surname} <br/>
      <b> Email poručioca: </b> ${this.orderForm.value.email} <br/>
      <b> Broj telefona poručioca: </b>  ${this.orderForm.value.phone} <br/>
      <b> Adresa poručioca: </b>  ${this.orderForm.value.Delivery.city}, ${this.orderForm.value.Delivery.street} ${this.orderForm.value.Delivery.number} <br/>
      <b> Dodatne informacije: </b> ${this.orderForm.value.Delivery.additionalInformation}
      `,
    });
  }

  sendOreder(cart: []) {
    this.order.street =
      this.orderForm.value.Delivery.city +
      " " +
      this.orderForm.value.Delivery.street;
    this.order.name = this.orderForm.value.name;
    this.order.surname = this.orderForm.value.surname;
    this.order.email = this.orderForm.value.email;
    this.order.phone = this.orderForm.value.phone;
    this.order.number = this.orderForm.value.Delivery.number;
    this.order.floor = this.orderForm.value.Delivery.floor;
    this.order.apartmant = this.orderForm.value.Delivery.apartment;
    this.order.additionalInformation = this.orderForm.value.Delivery.additionalInformation;
    this.store
      .dispatch(new SendOrder(this.order))
      .subscribe((or: RootState) => {
        const ord = or.orders.lastOrder;
        cart.map((prod) => {
          this.store.dispatch(new SaveOrderedProduct(ord.id, prod));
        });
        this.store.dispatch(new RemoveAllFromCart());
        Swal.fire({
          position: "bottom-end",
          icon: "success",
          title: "Porudžbina je uspešno poslata !",
          showConfirmButton: false,
          timer: 1500,
        });
        this.router.navigate(["/visitor/homepage"]);
      });
  }
}
