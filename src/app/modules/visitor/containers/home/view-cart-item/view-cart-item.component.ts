import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { ChangeAmount, RemoveItem } from 'src/app/store/products-store/products-actions';

@Component({
  selector: 'app-view-cart-item',
  templateUrl: './view-cart-item.component.html',
  styleUrls: ['./view-cart-item.component.css']
})
export class ViewCartItemComponent implements OnInit {
  @Input() product;
  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

  removeItem(productID: number) {
    this.store.dispatch(new RemoveItem(productID));
  }

  addAmount() {
    console.log(this.product);
    const newProd = {
      name: this.product.name,
      amount: this.product.amount + 1,
      size: this.product.size,
      description: this.product.description,
      price: this.product.price,
      imageUrl: this.product.imageUrl
    };
    this.store.dispatch(new ChangeAmount(+1, this.product, newProd));
  }

  lowerAmount() {
    if (this.product.amount !== 1) {
      const newProd = {
        name: this.product.name,
        amount: this.product.amount - 1,
        size: this.product.size,
        description: this.product.description,
        price: this.product.price,
        imageUrl: this.product.imageUrl
      };
      this.store.dispatch(new ChangeAmount(-1, this.product, newProd));
    } else {
      console.log('Cannot buy under 1 product !');
    }
  }

}
