import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageContainerComponent } from './mainpage-container/mainpage-container.component';
import { ViewCartContainerComponent } from './view-cart-container/view-cart-container.component';
import { CheckoutContainerComponent } from './checkout-container/checkout-container.component';
import { DisplayProductsContainerComponent } from './display-products-container/display-products-container.component';


const homeRoutes: Routes = [
  {
    path: '',
    children: [
      { path: 'homepage', component: MainpageContainerComponent },
      { path: 'cart', component: ViewCartContainerComponent },
      { path: 'checkout', component: CheckoutContainerComponent },
      { path: 'category/:category', component: DisplayProductsContainerComponent },
      { path: '**', redirectTo: '/visitor/homepage', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
export const homeRouting: ModuleWithProviders = RouterModule.forChild(homeRoutes);
