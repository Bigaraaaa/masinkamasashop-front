import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeRoutingModule, homeRouting } from "./home-routing.module";
import { MainpageContainerComponent } from "./mainpage-container/mainpage-container.component";
import { SharedModule } from "src/app/shared/shared.module";
import { BrandsComponent } from "../../components/brands/brands.component";
import { ViewCartContainerComponent } from "./view-cart-container/view-cart-container.component";
import { CheckoutContainerComponent } from "./checkout-container/checkout-container.component";
import { ViewCartItemComponent } from "./view-cart-item/view-cart-item.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DisplayProductsContainerComponent } from "./display-products-container/display-products-container.component";

@NgModule({
  declarations: [
    MainpageContainerComponent,
    BrandsComponent,
    ViewCartContainerComponent,
    CheckoutContainerComponent,
    ViewCartItemComponent,
    DisplayProductsContainerComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    homeRouting,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class HomeModule {}
