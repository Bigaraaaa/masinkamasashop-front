import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainpageContainerComponent } from './mainpage-container.component';

describe('MainpageContainerComponent', () => {
  let component: MainpageContainerComponent;
  let fixture: ComponentFixture<MainpageContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainpageContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainpageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
