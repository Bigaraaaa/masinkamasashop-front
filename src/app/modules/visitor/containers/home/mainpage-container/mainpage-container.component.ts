import { Component, OnInit, AfterViewInit, AfterContentInit } from '@angular/core';
import { Product } from 'src/app/core/models/product';
import { Select, Store } from '@ngxs/store';
import { ProductsState } from 'src/app/store/products-store/products-state';
import { Observable } from 'rxjs';
import { FilterProductCategory, GetProducts } from 'src/app/store/products-store/products-actions';
import { RootState } from 'src/app/store/rootstate';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mainpage-container',
  templateUrl: './mainpage-container.component.html',
  styleUrls: ['./mainpage-container.component.css']
})
export class MainpageContainerComponent implements OnInit {
  @Select(ProductsState.getProducts) products$: Observable<any>;
  allProducts;
  hairProducts = [];
  bodyProducts = [];
  faceProducts = [];
  aromatherapy = [];

  constructor(
    private store: Store,
    private router: Router
  ) { }

  ngOnInit() {
    console.log('main page');
    this.store.dispatch(new GetProducts()).subscribe(async rootState => {
      this.allProducts = await rootState.products.products;
      this.hairProducts = await this.allProducts.filter((pr) => {
        return pr.productDetails[0].categoryName === 'Nega Kose';
      });
      this.bodyProducts = await this.allProducts.filter((pr) => {
        return pr.productDetails[0].categoryName === 'Nega Tela';
      });
      this.faceProducts = await this.allProducts.filter((pr) => {
        return pr.productDetails[0].categoryName === 'Nega Lica';
      });
      this.aromatherapy = await this.allProducts.filter((pr) => {
        return pr.productDetails[0].categoryName === 'Aromaterapija';
      });
    });
  }

  filterProductCategory(category: string) {
    this.store.dispatch(new FilterProductCategory(category));
    // this.routing('visitor/category');
    this.router.navigate(['visitor/category', category]);
  }

  // get4fromCategory(categoryName) {
  //   return this.allProducts.filter((product: any) => {
  //     return product.productDetails[0].categoryName === categoryName;
  //   });
  // }

}
