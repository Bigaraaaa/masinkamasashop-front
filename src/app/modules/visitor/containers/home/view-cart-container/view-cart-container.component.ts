import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { RootState } from 'src/app/store/rootstate';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-cart-container',
  templateUrl: './view-cart-container.component.html',
  styleUrls: ['./view-cart-container.component.css']
})
export class ViewCartContainerComponent implements OnInit {
  products;
  sum;

  constructor(
    private store: Store,
    private router: Router
  ) { }

  ngOnInit() {
    this.store.select((rootState: RootState) => rootState.products.cart).subscribe((cart) => {
      this.products = cart;
      this.sum = 0;
      cart.map((el) => {
        this.sum += el.price * el.amount;
      });
    });
  }

  routing(route: string) {
    this.router.navigate([route]);
  }

}
