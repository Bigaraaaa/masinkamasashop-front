import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeContainerComponent } from './containers/home-container/home-container.component';


const visitorRoutes: Routes = [
  {
    path: '',
    component: HomeContainerComponent,
    // loadChildren: './containers/home/home.module#HomeModule'
    children: [
      { path: '', loadChildren: './containers/home/home.module#HomeModule' },
      { path: '**', redirectTo: 'homepage', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(visitorRoutes)],
  exports: [RouterModule]
})
export class VisitorRoutingModule { }
export const visitorRouting: ModuleWithProviders = RouterModule.forChild(visitorRoutes);
