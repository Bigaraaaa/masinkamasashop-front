import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeContainerComponent } from './containers/home-container/home-container.component';
import { InfoMenuComponent } from './components/info-menu/info-menu.component';
import { VisitorMenuComponent } from './components/visitor-menu/visitor-menu.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrandsComponent } from './components/brands/brands.component';
import { CartComponent } from './components/cart/cart.component';
import { CartViewComponent } from './components/cart-view/cart-view.component';
import { CartItemComponent } from './components/cart-item/cart-item.component';
import { visitorRouting } from './visitor-routing.module';
import { MainpageContainerComponent } from './containers/home/mainpage-container/mainpage-container.component';



@NgModule({
  declarations: [
    HomeContainerComponent,
    InfoMenuComponent,
    VisitorMenuComponent,
    // MainpageContainerComponent,
    // BrandsComponent,
    CartComponent,
    CartViewComponent,
    CartItemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    visitorRouting
  ],
  exports: [
    CartComponent,
    // BrandsComponent
  ]
})
export class VisitorModule { }
