import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/core/models/product';
import { Store } from '@ngxs/store';
import { RemoveItem } from 'src/app/store/products-store/products-actions';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {
  @Input() product: any;

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

  removeItem(product) {
    this.store.dispatch(new RemoveItem(product));
  }

}
