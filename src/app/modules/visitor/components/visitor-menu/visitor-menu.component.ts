import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { FilterProductCategory } from 'src/app/store/products-store/products-actions';

@Component({
  selector: 'app-visitor-menu',
  templateUrl: './visitor-menu.component.html',
  styleUrls: ['./visitor-menu.component.css']
})
export class VisitorMenuComponent implements OnInit {

  constructor(
    private router: Router,
    private store: Store
  ) { }

  ngOnInit() {
  }

  routing(route: string) {
    this.router.navigate([route]);
  }

  filterProductCategory(category: string) {
    this.store.dispatch(new FilterProductCategory(category));
    // this.routing('visitor/category');
    this.router.navigate(['visitor/category', category]);
  }

}
