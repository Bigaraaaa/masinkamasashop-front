import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { RootState } from 'src/app/store/rootstate';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  products = [];
  sum = 0;

  constructor(
    private store: Store,
    private router: Router
  ) { }

  ngOnInit() {
    this.store.select((rootState: RootState) => rootState.products.cart).subscribe((cart) => {
      this.products = cart;
      this.sum = 0;
      cart.map((el) => {
        this.sum += el.price;
      });
    });
  }

  routing(route: string) {
    this.router.navigate([route]);
  }

}
