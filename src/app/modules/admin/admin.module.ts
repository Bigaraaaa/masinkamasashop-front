import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayOrdersComponent } from './components/display-orders/display-orders.component';
import { adminRouting } from './admin-routing.module';
import { NewProductComponent } from './components/new-product/new-product.component';
import { DisplayProductsComponent } from './components/display-products/display-products.component';
import { AdminMenuComponent } from './components/admin-menu/admin-menu.component';
import { DashboardContainerComponent } from './containers/dashboard-container/dashboard-container.component';
import { ChartsModule } from 'ng2-charts';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { FormsModule } from '@angular/forms';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';


@NgModule({
  declarations: [DisplayOrdersComponent, NewProductComponent, DisplayProductsComponent,
    AdminMenuComponent, DashboardContainerComponent, OrderDetailsComponent, AdminLoginComponent],
  imports: [
    CommonModule,
    adminRouting,
    ChartsModule,
    FormsModule
  ],
  exports: [
    AdminMenuComponent
  ]
})
export class AdminModule { }
