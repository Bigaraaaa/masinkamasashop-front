import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { GetProducts } from 'src/app/store/products-store/products-actions';
import { ProductsState } from 'src/app/store/products-store/products-state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard-container',
  templateUrl: './dashboard-container.component.html',
  styleUrls: ['./dashboard-container.component.css']
})
export class DashboardContainerComponent implements OnInit {
  public barChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'line';
  public barChartLegend = true;

  public barChartData = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Price' },
    { data: [45, 39, 60, 41, 46, 35, 15], label: 'Cost' }
  ];


  constructor(
    private store: Store
  ) { }

  ngOnInit() {
    this.store.dispatch(new GetProducts());
  }

}
