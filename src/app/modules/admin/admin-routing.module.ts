import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DisplayOrdersComponent } from './components/display-orders/display-orders.component';
import { DisplayProductsComponent } from './components/display-products/display-products.component';
import { DashboardContainerComponent } from './containers/dashboard-container/dashboard-container.component';
import { NewProductComponent } from './components/new-product/new-product.component';
import { ProductCardComponent } from 'src/app/shared/components/product-card/product-card.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';

const adminRoutes: Routes = [
  {
    path: '',
    children: [
      // { path: 'menu', component: AdminMenuComponent, canActivate: [AuthGuard], data: { expectedRoles: ['ROLE_ADMINISTRATOR']}},
      { path: 'orders', component: DisplayOrdersComponent },
      { path: 'products', component: DisplayProductsComponent },
      { path: 'dashboard', component: DashboardContainerComponent },
      { path: 'new-product', component: NewProductComponent },
      { path: 'order-details', component: OrderDetailsComponent },
      { path: 'login', component: AdminLoginComponent },
      { path: '**', redirectTo: '/admin/dashboard', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(adminRoutes)
  ]
})
export class AdminRoutingModule { }
export const adminRouting: ModuleWithProviders = RouterModule.forChild(adminRoutes);
