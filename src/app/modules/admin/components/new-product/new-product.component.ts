import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
} from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ProductsService } from "src/app/core/services/admin-services/products/products.service";
import { Store, Select } from "@ngxs/store";
import {
  SaveProduct,
  SaveSizes,
} from "src/app/store/products-store/products-actions";
import { Product } from "src/app/core/models/product";
import { ProductsState } from "src/app/store/products-store/products-state";
import { Observable } from "rxjs";
import Swal from "sweetalert2";
import { Categories } from "src/app/core/models/categories";

@Component({
  selector: "app-new-product",
  templateUrl: "./new-product.component.html",
  styleUrls: ["./new-product.component.css"],
})
export class NewProductComponent implements OnInit {
  @Select(ProductsState.getLastAdded) $lastAdded: Observable<any>;
  @ViewChild("divParent", { static: false }) div: ElementRef;
  categories = [];
  product = {
    name: "",
    amount: null,
    description: "",
    price: null,
    categoryName: "",
  };
  sizeAmount = [
    {
      id: 0,
      amount: "",
      price: 0.0,
      type: "",
    },
  ];

  constructor(private store: Store) {}

  ngOnInit() {
    this.categories = Object.values(Categories).map((el) => {
      return el;
    });
  }

  removeSizeAmount(index: number) {
    this.sizeAmount = this.sizeAmount.filter((el) => {
      return el.id !== index;
    });
    console.log(this.sizeAmount, index);
  }

  createNew() {
    console.log(this.sizeAmount);
    this.sizeAmount.push({
      id: this.sizeAmount.length,
      amount: "",
      price: 0.0,
      type: "",
    });
  }

  saveProduct() {
    console.log(this.product);
    this.store.dispatch(new SaveProduct({}, this.product));
    // Brisanje atributa iz objekta
    // delete this.sizeAmount[0].id;
    // console.log(this.sizeAmount);
  }

  async saveSizes(prodDet) {
    await console.log(this.sizeAmount);
    await this.sizeAmount.map((size) => {
      delete size.id;
      this.store.dispatch(new SaveSizes(prodDet.id, size));
    });
    await Swal.fire({
      position: "bottom",
      icon: "success",
      title: "Veličine su uspešno dodate !",
      showConfirmButton: false,
      timer: 2000,
    });
  }
}
