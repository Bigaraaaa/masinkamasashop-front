import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/core/services/admin-services/orders/orders.service';
import { Store, Select } from '@ngxs/store';
import { GetOrders, SelectOrder } from 'src/app/store/orders-store/orders-actions';
import { OrdersState } from 'src/app/store/orders-store/orders-state';
import { Observable } from 'rxjs';
import { Orders } from 'src/app/core/models/orders';
import { Router } from '@angular/router';

@Component({
  selector: 'app-display-orders',
  templateUrl: './display-orders.component.html',
  styleUrls: ['./display-orders.component.css']
})
export class DisplayOrdersComponent implements OnInit {
  @Select(OrdersState.getOrders) $orders: Observable<any[]>;

  constructor(
    private store: Store,
    private router: Router
  ) { }

  ngOnInit() {
    this.store.dispatch(new GetOrders());
  }

  selectOrder(order: Orders) {
    this.store.dispatch(new SelectOrder(order));
    this.router.navigate(['admin/order-details']);
  }

}
