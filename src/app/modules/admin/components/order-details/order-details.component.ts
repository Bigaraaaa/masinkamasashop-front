import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { OrdersState } from 'src/app/store/orders-store/orders-state';
import { Select, Store } from '@ngxs/store';
import { Orders } from 'src/app/core/models/orders';
import { FufillOrder } from 'src/app/store/orders-store/orders-actions';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent {
  @Select(OrdersState.getSelectedOrder) $selectedOrder: Observable<Orders>;

  constructor(
    private store: Store
  ) { }

  fufillOrder(orderID) {
    console.log('fufillOrder');
    this.store.dispatch(new FufillOrder(orderID));
  }
}
