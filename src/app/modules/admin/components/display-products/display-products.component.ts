import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ProductsState } from 'src/app/store/products-store/products-state';

@Component({
  selector: 'app-display-products',
  templateUrl: './display-products.component.html',
  styleUrls: ['./display-products.component.css']
})
export class DisplayProductsComponent implements OnInit {
  @Select(ProductsState.getProducts) $allProducts: Observable<any>;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  newProduct() {
    this.router.navigate(['admin/new-product']);
  }

}
