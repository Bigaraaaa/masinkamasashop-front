import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "src/app/core/auth/auth.service";
import { ProductsService } from "src/app/core/services/admin-services/products/products.service";

@Component({
  selector: "app-admin-menu",
  templateUrl: "./admin-menu.component.html",
  styleUrls: ["./admin-menu.component.css"],
})
export class AdminMenuComponent implements OnInit {
  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private productService: ProductsService
  ) {}

  ngOnInit() {}

  routing(link: string) {
    this.router.navigate(["admin/" + link]);
  }

  test() {
    this.productService.getProducts().subscribe((data) => console.log(data));
  }

  onLogOut() {
    this.auth.logout();
  }
}
