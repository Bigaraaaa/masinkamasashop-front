import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Login } from 'src/app/store/connection-store/connection-actions';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  user;
  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

  login() {
    this.store.dispatch(new Login(this.user));
  }

}
